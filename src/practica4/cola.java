/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica4;

/**
 *
 * @author EVERT NICACIO PACHECO     CI:7450101
 */
public class cola {
    private pila p1, p2;
    public cola(){
       p1 = new pila();
       p2 = new pila();
    }
    public void introducirDatos(int dato){
       while (!p1.isEmpty()){
          p2.push(p1.pop());
       }
       p1.push(dato);
       while (!p2.isEmpty()){
          p1.push(p2.pop());
       }
    }
    public boolean vacio(){
       return (p1.isEmpty());
    }
    public int sacarDato(){
       int temp = -1;
       if(this.vacio())
           System.out.println("cola vacia");
       else 
           temp = p1.pop();
       return temp;
    }
    public int primero(){
        int temp = p1.pop();
        System.out.println("el primero es");
        return temp;
        
    }
    
}

